;;; sgfv.scm

;; Copyright (C) 2012, 2013 Thien-Thi Nguyen
;;
;; This file is part of SGF Utils.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with SGF Utils.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Usage: sgfv [options] SGF-FILE
;;
;; Graphically browse one of the Go games in SGF-FILE.
;; Options are (defaults in square braces):
;;
;;  -g, --geometry WxH    -- Use window W by H pixels [900x600]
;;  -n, --number N        -- Select game N in SGF-FILE [1]
;;  -c, --config FILENAME -- Read configuration from FILENAME
;;                           [$HOME/.sgf-utils/sgfv]
;;
;; Keys:
;;
;;  q       -- quit (also ‘C-q’, ‘ESC’)
;;  C-h     -- display key bindings
;;  f       -- show final info
;;  0..9    -- prefix arg
;;  C-g     -- cancel prefix arg
;;  C-l     -- refresh
;;  n [N]   -- next node; prefix arg selects
;;             among variations (default 1)
;;  v [N]   -- cycle through sibling variations;
;;             prefix arg specifies variation N
;;  C-n [N] -- skim next; prefix arg like ‘n’
;;  .       -- (period) jump to end; prefix arg like ‘n’
;;  p       -- previous node
;;  C-p     -- skim previous
;;  ,       -- (comma) jump to root
;;  b       -- cycle "btw" indicator:
;;               select (default)
;;               mark
;;               circle
;;               square
;;               triangle
;;               (none)
;;  a       -- cycle "ghost" (sibling variations)
;;             indicator; like ‘b’
;;  s       -- cycle what to show:
;;               everything (default)
;;               stones/comment
;;               stones
;;  /       -- display game name/result/players/etc (metainfo)
;;
;; A "btw" is the list of positions mentioned in a comment.
;; For example, comment "C3 is ok, but A4 is better" mentions
;; two positions: C3 and A4.  These are indicated on the board.
;;
;; When there are multiple next nodes possible (variations),
;; these are numbered and indicated on the board labeled with
;; a white pinstripe.  (Normal labels have a red pinstripe.)
;; You can use the prefix arg to a follow variations 2 and up,
;; or jump directly from one variation to its sibling.
;;
;; To skim is to continue in the specified direction, stopping
;; only at "interesting" nodes: root, end, hot spot, variation,
;; and/or fork (a fork is the node that precedes a variation).

;;; Code:

(define-module (sgfv)
  #:export (main)
  #:use-module (ice-9 curried-definitions))

(use-modules
 ((base) #:select (FE shunt fs))
 ((bgx-banalities) #:select (chv
                             qop<-args))
 ((ff-data) #:select (look-up-property))
 ((ff-r) #:select (read-sgf
                   nna
                   children
                   nodes
                   node-move
                   get-one))
 ((render) #:select (make-screen))
 ((gnugo) #:select (gnugo-via-gtp))
 ((sdl sdl) #:prefix SDL:)
 ((sdl misc-utils) #:prefix MISC:)
 ((srfi srfi-1) #:select (circular-list
                          drop-while
                          take-while!
                          filter
                          filter!
                          list-index
                          remove
                          partition!))
 ((srfi srfi-2) #:select (and-let*))
 ((srfi srfi-9) #:select (define-record-type))
 ((srfi srfi-11) #:select (let-values))
 ((srfi srfi-13) #:select (substring/shared
                           string-tokenize
                           string-trim-both
                           string-drop-right
                           string-join
                           string-prefix?
                           string-prefix-ci?))
 ((srfi srfi-14) #:select (char-set-complement
                           char-set))
 ((ice-9 regex) #:select (match:substring
                          list-matches)))

(define (fop<- from)
  (let ((cfg (or (false-if-exception
                  (with-input-from-port
                      (let ((from (if (symbol? from)
                                      (fs "~A/.sgf-utils/~A"
                                          (getenv "HOME")
                                          from)
                                      from)))
                        (if (string=? "-" from)
                            (current-input-port)
                            (open-input-file from)))
                    (lambda ()
                      ;; Config file format is "plist w/o surrounding parens".
                      ;; On input, convert it internally to an alist.
                      (let loop ((acc '()))
                        (let ((k (read)))
                          (if (eof-object? k)
                              (begin
                                (close-port (current-input-port))
                                acc)
                              (loop (acons k (read) acc))))))))
                 '())))
    ;; rv
    (lambda (k)
      (assq-ref cfg k))))

(define family (make-object-property))
(set! (family #f) '())

(define parent (make-object-property))

(define sprout (make-object-property))

(define move-number (make-object-property))

(define dimmed (make-object-property))
(define mememe (make-object-property))

(define-record-type !DAG!
    (make-dag root tips mids source)
    dag?
  (root dag-root)
  (tips dag-tips)
  (mids dag-mids)
  (source dag-source-filename))

(define (four-sight-ful? node)          ; yuk yuk
  (not (eq? (family (parent node))
            (family node))))

(define (grok source tree)              ; => !DAG!
  (let ((first #f)
        (mid '())
        (tip '()))

    (define (preceding-move-number node) ; => ‘#f’ or INTEGER
      (and node (or (move-number node)
                    (preceding-move-number
                     (parent node)))))

    (define (walk prev tree)
      (let loop ((prev prev) (ls (nodes tree)))
        (cond ((pair? ls)
               (let ((node (car ls)))

                 (define (inherit! prop objprop)
                   (set! (objprop node)
                         (cond ((nna (get-one node prop))
                                => (lambda (places)
                                     (and (pair? places)
                                          (car places)
                                          places)))
                               (else
                                ;; inherit
                                (objprop prev)))))

                 (cond ((not prev)
                        (set! first node))
                       ((sprout prev))
                       (else (set! (sprout prev) node)))
                 (or (family node)
                     (set! (family node) (family prev)))
                 (set! (parent node) prev)
                 (and (node-move node)
                      (set! (move-number node)
                            (cond ((get-one node 'MN))
                                  ((preceding-move-number (parent node)) => 1+)
                                  (else 1))))
                 (inherit! 'DD dimmed)
                 (inherit! 'VW mememe)
                 (loop node (cdr ls))))
              ((children tree)
               => (lambda (kids)
                    (set! mid (cons prev mid))
                    (let ((ancestry (family prev))
                          (firsts (map car (map nodes kids)))
                          (count (length kids)))
                      (set! (sprout prev) (list->vector firsts))
                      (FE firsts (iota count)
                          (lambda (node idx)
                            (set! (family node) (cons idx ancestry))))
                      (FE (make-list count prev)
                          kids
                          walk))))
              (else
               (set! tip (cons prev tip))))))

    ;; do it!
    (walk #f tree)

    ;; rv
    (make-dag first
              (reverse! tip)
              (reverse! mid)
              source)))

(define (move-details node)
  (let ((pair (or (node-move node)      ; (WHO . POS)
                  '(#f . #f))))
    (values (move-number node)
            (car pair)
            (cdr pair))))

;;; focus

(define (nfna ls)                       ; null/false not appreciated
  (nna (delq #f ls)))

(define (node-property-places node prop)
  (nna (get-one node prop)))

(define (focus-manager BQ dag)
  (let* ((root (dag-root dag))
         (focus root)
         (was #f))

    (define (at)
      focus)

    (define (synch!)
      (BQ 'loadsgf (dag-source-filename dag) 1))

    (define (at-root?)
      (eq? focus root))

    (define (ok! now)
      (set! was focus)
      (set! focus now))

    (define (forw! sel)
      (and-let* ((maybe (sprout focus))
                 (forw (if (vector? maybe)
                           (vector-ref
                            maybe (if (and sel (positive? sel))
                                      (1- (min sel (vector-length maybe)))
                                      0))
                           maybe)))
        (let-values (((n who pos) (move-details forw)))
          (ok! forw)
          (cond (n (and (= 1 n)
                        (synch!))
                   (BQ 'play who pos)))
          focus)))

    (define (back!)
      (and-let* ((back (parent focus)))
        (and (move-number focus)
             (BQ 'undo))
        (ok! back)
        (and (at-root?)
             (BQ 'clear_board))
        focus))

    (define (vary! how)
      (let ((v (sprout (parent focus))))
        (if (not (vector? v))
            "no variations"
            (let* ((count (vector-length v))
                   (sel (cond (how => 1-)
                              (else (modulo (1+ (car (family focus)))
                                            count)))))
              (cond ((not (< -1 sel count))
                     (fs "no such variation: ~A" how))
                    (else
                     (back!)
                     (forw! (1+ sel))))))))

    (define (beg!)
      (let loop ()
        (and (back!)
             (loop)))
      focus)

    (define (end! sel)
      (let loop ((sel sel))
        (if (forw! sel)
            (loop #f)
            (values focus (let ((tips (dag-tips dag)))
                            (and (pair? (cdr tips))
                                 (1+ (list-index (lambda (node)
                                                   (eq? focus node))
                                                 tips))))))))

    (define (multi-futures)
      (and-let* ((maybe (sprout focus))
                 ((vector? maybe)))
        (nfna (map (lambda (node)
                     (and (move-number node)
                          (or (assq-ref node 'B)
                              (assq-ref node 'W))))
                   (vector->list maybe)))))

    ;; prep
    (synch!)

    ;; rv
    (shunt (at forw! back! vary!
               beg! end!
               multi-futures))))

(define (valid-go-game tree)
  (and-let* ((ls (nodes tree))
             ((not (null? ls)))
             (first (car ls))
             ;; GM[1] => go
             ((= 1 (get-one first 'GM))))
    (get-one first 'SZ)))

(define (cycler . elements)
  (let ((valid (apply circular-list elements)))
    ;; rv
    (lambda args
      (or (null? args) (set! valid (cdr valid)))
      (car valid))))

(define showing (cycler 'everything
                        'stones/comment
                        'stones))

(define (usual-i)
  (cycler 'select!
          'mark!
          'circle!
          'square!
          'triangle!
          #f))

(define (pretty-i i)
  (if i
      (string-drop-right
       (symbol->string i)
       1)
      '(none)))

(define btw-indicator (usual-i))

(define ghost-indicator (usual-i))

(define ((adj-c as) c fop)
  (and-let* ((nm (procedure-name c))
             (to (fop nm)))
    (let ((want ((or as identity) to))
          (orig (c)))
      (let loop ()
        (cond ((eq? want (c)))
              (else (and (eq? orig (c #t))
                         (error (fs "bad ‘~A’ value: ~S"
                                    nm to)))
                    (loop)))))))

(define adj-i (adj-c (lambda (to)
                       (case to
                         ((none) #f)
                         (else (symbol-append to '!))))))

(define mentioned
  (let ((rx (make-regexp "[A-Z][0-9]+")))
    ;; mentioned
    (lambda (size BQ)

      (define check
        (if (pair? size)
            ;; rectangular
            (let ((cols (car size))
                  (rows (cdr size)))
              (lambda (v)
                (and (> cols (vector-ref v 0))
                     (> rows (vector-ref v 1)))))
            ;; square
            (lambda (v)
              (and (> size (vector-ref v 0))
                   (> size (vector-ref v 1))))))

      (define (infer-positions m)
        (BQ 'position<-cc (match:substring m)))

      ;; rv: proc => ‘#f’ or (POSITION ...)
      (lambda (text)
        (and=> (nna (list-matches rx text))
               (lambda (ls)
                 (nna (filter! check (map infer-positions ls)))))))))

(define lines
  (let ((cs (char-set-complement (char-set #\newline))))
    ;; lines
    (lambda (string)
      (map string-trim-both (string-tokenize string cs)))))

(define (pretty-result s)
  (or
   ;; standard constant
   (assoc-ref '(("0" . "Draw")
                ("Void" . "No Result / Suspended Play")
                ("?" . "Unknown"))
              s)
   ;; standard variable
   (and-let* (((<= 2 (string-length s)))
              ((char=? #\+ (string-ref s 1)))
              (who (assoc-ref '((#\B . Black)
                                (#\W . White))
                              (string-ref s 0))))
     (let ((init (fs "~A wins" who))
           (rest (substring/shared s 2)))

       (define (plus more)
         (string-append init " " more))

       (cond ((string-null? rest) init)
             ((member rest '("R" "Resign")) (plus "by resignation"))
             ((member rest '("T" "Time")) (plus "on time"))
             ((member rest '("F" "Forfeit")) (plus "by forfeit"))
             (else (plus (fs "by ~A" rest))))))
   ;; wtf
   (fs "(RE: ~A)" s)))

(define (metainfo root n-tips n-branches) ; => (STRING ...)

  (define not-applicable
    (let ((n/a '(FF GM SZ AB AW C)))
      ;; not-applicable
      (lambda (k/v)
        (memq (car k/v) n/a))))

  (let ((copy (remove not-applicable root))
        (blurbs '())
        (ignorable '((RU . "Japanese")  ; TODO: runtime user config
                     (EV . "None"))))

    (define (pr prop)
      (and=> (get-one copy prop)
             (lambda (value)
               (set! copy (assq-remove! copy prop))
               (and (not (member (cons prop value) ignorable))
                    value))))

    (define (chk prop proc)
      (and=> (pr prop) proc))

    (define (b-one! s)
      (set! blurbs (cons s blurbs)))

    (define (blurb! s . args)
      (b-one! (apply fs s args)))

    (define (empty!)
      (b-one! " "))

    (define (name/rank/team who)

      (define (player symbol)
        (symbol-append who symbol))

      (string-append
       (or (pr (symbol-append 'P who))
           "(anonymous)")
       (or (and=> (pr (player 'R))
                  (lambda (rank)
                    (fs " (~A)" rank)))
           "")
       (or (and=> (pr (player 'T))
                  (lambda (team)
                    (fs ", team: ~A" team)))
           "")))

    (define (slash-sep ls)
      (string-join ls " / "))

    (define (pretty prop)
      (car (look-up-property prop)))

    (define ((named-prop prop) s)
      (fs "~A: ~A" (pretty prop) s))

    ;; game
    (let ((name (or (chk 'GN (lambda (s) (string-append "“" s "”")))
                    "(unnamed game)"))
          (time (pr 'DT))
          (loc (nfna (map pr '(PC EV RO))))
          (etc (nfna (map (lambda (prop name)
                            (chk prop (lambda (value)
                                        (fs "~A ~A" value name))))
                          '(RU KM TM OT)
                          '(rules komi "s (time limit)" (on overtime))))))
      (b-one! name)
      (and=> time b-one!)
      (and loc (b-one! (string-join loc ", ")))
      (and etc (b-one! (slash-sep etc)))
      (chk 'RE (lambda (result)
                 (empty!)
                 (blurb! "•  ~A  •" (pretty-result result)))))
    (empty!)
    ;; players
    (b-one! "(B)")
    (blurb! "~A~A"
            (name/rank/team 'B)
            (or (chk 'HA (lambda (handicap)
                           (and (not (zero? handicap))
                                (fs ", handicap ~A" handicap))))
                ""))
    (b-one! "(W)")
    (b-one! (name/rank/team 'W))
    ;; opening / game commentary
    (let ((opening (chk 'ON (named-prop 'ON)))
          (comment (chk 'GC lines)))
      (and-let* ((all (nfna (cons opening (or comment '())))))
        (empty!)
        (FE all (lambda (line)
                  (b-one! line)))))
    ;; credits
    (let* ((us (pr 'US))
           (ap (chk 'AP (lambda (pair)
                          (fs "~A ~A" (car pair) (cdr pair)))))
           (so (chk 'SO (named-prop 'SO)))
           (an (chk 'AN (named-prop 'AN)))
           (cp (chk 'CP (lambda (s)
                          (if (string-prefix-ci? "copyright" s)
                              s
                              (string-append "© " s)))))
           (one (and=> (nfna (list us ap)) slash-sep))
           (two (and=> (nfna (list so an)) slash-sep)))
      (and-let* ((all (nfna (list one two cp))))
        (empty!)
        (FE all b-one!)))
    ;; misc: iffy and wtf!
    (or (null? copy)
        (let-values (((iffy wtf!) (partition!
                                   (lambda (k/v)
                                     (look-up-property (car k/v)))
                                   copy)))
          (and-let* ((all (nfna (append iffy wtf!))))
            (empty!)
            (b-one! "(etc)")
            (FE (map car all)
                (map cdr all)
                (lambda (prop value)
                  (let ((bef (if (assq prop iffy)
                                 (string-append (pretty prop) " ")
                                 ""))
                        (aft (fs "[~A] -- ~A" prop value)))
                    (blurb! "~A~A" bef aft)))))))
    ;; nonlinearity
    (cond ((zero? n-branches))
          (else (empty!)
                (blurb! "[ ~A fork~A / ~A ends ]"
                        n-branches
                        (if (= 1 n-branches)
                            ""
                            "s")
                        n-tips)))
    ;; rv
    (reverse! blurbs)))

(define (keybindings)                   ; => (STRING ...)

  (define (all)
    (lines (with-output-to-string
             (lambda ()
               (chv (list (car (command-line)) "--help")
                    'no-exit)))))

  (define (bef s)
    (not (string=? "Keys:" s)))

  (define (aft s)
    (not (string-prefix? "A \"btw\"" s)))

  (map (lambda (line)
         (string-join (string-tokenize line)))
       (take-while! aft (cdr (drop-while bef (all))))))

(define context
  (let ((l-cache '())
        (s-cache (vector)))

    (define (lineage node)
      (and-let* ((ls (nna (family node))))
        (or (assoc-ref l-cache ls)
            (let ((pretty (string-append
                           "v" (string-join (map number->string
                                                 (map 1+ (reverse ls)))
                                            "."
                                            'prefix))))
              (set! l-cache (acons ls pretty l-cache))
              pretty))))

    (define (s-count v)
      (let ((num (vector-length v)))
        (or (<= num (vector-length s-cache))
            (set! s-cache (list->vector (map (lambda (n)
                                               (fs "/~A" (1+ n)))
                                             (iota num)))))
        (vector-ref s-cache (1- num))))

    ;; context
    (lambda (node)
      (let* ((lin (lineage node))
             (sib (and-let* ((lin)
                             (v (sprout (parent node)))
                             ((vector? v)))
                    v)))
        (let-values (((n who pos) (move-details node)))
          (values (if sib
                      (string-append lin (s-count sib))
                      lin)
                  sib n who pos))))))

(define (ponder BQ S TIQ fop)

  (define (message! s . args)
    (S 'message! (apply fs s args)))

  (define (stones! b w)
    (S 'stones! b w))

  (let* ((mind (focus-manager BQ TIQ))
         (sz (get-one (dag-root TIQ) 'SZ))
         (properly-mentioned (mentioned sz BQ))
         (places #f)
         (black #f) (white #f)
         (meta #f) (pos #f)
         (overlays #f))

    (define (hey! ls)
      (S 'display-blurbs! ls))

    (define (display-metainfo!)
      (hey! (metainfo (dag-root TIQ)
                      (length (dag-tips TIQ))
                      (length (dag-mids TIQ)))))

    (define (ov-add! key val)
      (set! overlays (acons key val overlays)))

    (define (ov key)
      (assq-ref overlays key))

    (define (compose! node)

      (define (meta! s . args)
        (set! meta (cons (apply fs s args) meta))
        #t)

      (define (one prop)
        (get-one node prop))

      (define (mumble prop yada)
        (and (one prop)
             (meta! yada)))

      (define (shriek prop yada)
        (and=> (one prop)
               (lambda (n)
                 (meta! "~A~A" (if (= 1 n)
                                   ""
                                   "very ")
                        yada))))

      (define (pretty prop fmt)
        (and=> (one prop)
               (lambda (value)
                 (meta! fmt value))))

      (define ((try extract) prop)
        (and=> (extract node prop)
               (lambda (positions)
                 (ov-add! prop positions))))

      (define (inherit! prop objprop)
        (and-let* ((positions (objprop node)))
          (ov-add! prop positions)))

      ;; do it!
      (set! overlays '())

      (FE '(TB TW SL MA CR SQ TR)
          (try node-property-places))

      (FE '(LN AR LB)
          (try get-one))

      (inherit! 'DD dimmed)
      (inherit! 'VW mememe)

      (and=> (get-one node 'C)
             (lambda (text)
               (ov-add! 'C (lines text))
               (and-let* ((positions (properly-mentioned text)))
                 (ov-add! 'btw (cons (btw-indicator) positions)))))

      ;; Order "add WHO" before ‘get-positions WHO’ because most of the
      ;; time, the latter count vastly outnumbers that of the former.
      ;; (So ‘append’ needs to cons less.)
      (set! black (append (or (one 'AB) '()) (BQ 'get-positions 'B)))
      (set! white (append (or (one 'AW) '()) (BQ 'get-positions 'W)))

      ;; These ‘meta!’ (and derived) calls are ordered bottom to top,
      ;; and displayed top to bottom, thus avoiding a ‘reverse!’.
      (set! places (mememe node))
      (set! meta '())
      (and places (meta! "(partial view)"))
      (or (shriek 'DM "even")
          (shriek 'GB "good for black")
          (shriek 'GW "good for white")
          (shriek 'UC "uncertain"))
      (let-values (((lin sib n who where) (context node)))
        (define (mov)
          (fs "(~A ~A) ~A"
              n who (BQ 'cc/PASS<-position where)))
        (and n (or (shriek 'BM "bad")
                   (mumble 'DO "doubtful")
                   (mumble 'IT "interesting")
                   (mumble 'TE "good")))
        (set! pos (if sib
                      (let loop ((ghosts '())
                                 (ls (reverse! (vector->list sib))))
                        (if (null? ls)
                            (lambda ()
                              (values ghosts who
                                      (ghost-indicator)
                                      where))
                            (let* ((sib (car ls))
                                   (cur? (eq? sib node))
                                   (pos (cdr (node-move sib))))
                              (meta! "  ~A  ~A  ~A"
                                     (1+ (car (family sib)))
                                     (if cur?
                                         #\!
                                         #\-)
                                     (BQ 'cc/PASS<-position pos))
                              (loop (if cur?
                                        ghosts
                                        (cons pos ghosts))
                                    (cdr ls)))))
                      where))
        (cond ((and (not lin) (not n)))
              ((not n) (meta! lin))
              ((not lin) (meta! (mov)))
              (else (meta! "~A ~A" lin (mov)))))
      (pretty 'V "value: ~A")
      (shriek 'HO "hot spot")
      (pretty 'N "~A")
      (and=> (mind 'multi-futures)
             (lambda (positions)
               (ov-add! 'multi-future-label! positions))))

    (define (show-final-info!)

      (define (now)
        (let ((pair (gettimeofday)))
          (+ (* (car pair) 1000000) (cdr pair))))

      (message! "(please wait …)")
      (let ((was overlays)
            (beg (now)))
        (let-values (((RE CB CW TB TW DD seki) (BQ 'final-info)))
          (ov-add! 'TB TB)
          (ov-add! 'TW TW)
          (ov-add! 'DD DD)
          (ov-add! 'SL seki)
          (ov-add! 'C (list (fs "B: ~A T + ~A C" (length TB) CB)
                            (fs "W: ~A T + ~A C" (length TW) CW)
                            (fs "~A" (pretty-result RE))))
          (refresh!)
          (set! overlays was)
          (let* ((dur (- (now) beg))
                 (unit (cond ((< 1000000 dur) 'sec)
                             ((< 1000 dur) 'msec)
                             (else 'usec))))
            (message! "(please wait … ~A ~A)"
                      (exact->inexact
                       (/ (truncate (* 1000 (/ dur (case unit
                                                     ((sec)  1000000)
                                                     ((msec) 1000)
                                                     (else   1)))))
                          1000))
                      unit)))))

    (define (refresh!)

      (define (chk key proc)
        (and=> (ov key) proc))

      (define (chk-C!)
        (chk 'C (lambda (ls)
                  (chk 'btw (lambda (pair)
                              (and-let* ((btw (car pair)))
                                (S btw (cdr pair)))))
                  (S 'comment! ls))))

      (S 'clear!)
      (S 'mememe! places)
      (stones! black white)
      (S 'display-meta! pos meta)

      (case (showing)
        ((everything)
         (let ((b (ov 'TB))
               (w (ov 'TW)))
           (and (or b w)
                (S 'territories! (or b '()) (or w '()))))
         (FE '((SL . select!)
               (MA . mark!)
               (CR . circle!)
               (SQ . square!)
               (TR . triangle!)
               (LN . segment!)
               (AR . ray!)
               ;; Add other stuff here.
               ;; (Keep ‘LB’ and ‘DD’ last.)
               (LB . label!)
               (DD . dim!))
             (lambda (pair)
               (chk (car pair) (lambda (positions)
                                 (S (cdr pair) positions)))))
         (FE '(multi-future-label!)
             (lambda (x)
               (chk x (lambda (positions)
                        (S x positions)))))
         (chk-C!))
        ((stones/comment)
         (chk-C!)))
      (S 'show!))

    (define (redraw-completely! node)
      (compose! node)
      (refresh!))

    (define (no-more!)
      (message! "(no more)"))

    (define (already-at-root!)
      (message! "(already at root)"))

    (define (forw! n)
      (cond ((mind 'forw! n) => redraw-completely!)
            (else (no-more!))))

    (define (back!)
      (cond ((mind 'back!) => redraw-completely!)
            (else (already-at-root!))))

    (define ((stop-or go-on! . args) node)
      (redraw-completely! node)
      (or (and=> (nfna (list (and (eq? (dag-root TIQ) node) "root")
                             (and (not (sprout node)) "end")
                             (and (get-one node 'HO) "hot spot")
                             (and (four-sight-ful? node) "variation")
                             (and (ov 'multi-future-label!) "fork")))
                 (lambda (ls)
                   (message! "(~A)" (string-join ls ", "))))
          (apply go-on! args)))

    (define (skim-forw! n)
      (cond ((mind 'forw! n) => (stop-or skim-forw! #f))
            (else (no-more!))))

    (define (skim-back!)
      (cond ((mind 'back!) => (stop-or skim-back!))
            (else (already-at-root!))))

    (BQ 'set-board-size! sz)
    (S 'set-board-size!
       ;; Sigh, SGF allows non-square boards, but GTP 2 doesn't.  Here's
       ;; to hoping this upward-compatability will someday be rewarded.
       (if (pair? sz)
           (car sz)
           sz)
       (if (pair? sz)
           (cdr sz)
           sz))

    (redraw-completely! (mind 'at))
    (display-metainfo!)

    (SDL:enable-key-repeat 250 42)

    (let ((ev (SDL:make-event))
          (prefix-arg #f))

      (define (get-prefix-arg)
        (let ((rv prefix-arg))
          (set! prefix-arg #f)
          rv))

      (define (bye!)
        #t)

      (MISC:ignore-all-event-types-except 'key-down
                                          'active
                                          'quit)
      (let loop ()
        (SDL:wait-event ev)
        (case (SDL:event:type ev)
          ((key-down)
           (let ((sym (SDL:event:key:keysym:sym ev)))
             (and (member (SDL:event:key:keysym:mod ev)
                          '((L-ctrl) (R-ctrl)))
                  (set! sym (string->symbol
                             (string-append
                              "C-" (symbol->string sym)))))
             (case sym
               ((escape q C-q)
                (bye!))
               ((f)
                (show-final-info!)
                (loop))
               ((D-0 D-1 D-2 D-3 D-4 D-5
                     D-6 D-7 D-8 D-9)
                (let ((number (string->number (substring/shared
                                               (symbol->string sym)
                                               2))))
                  (set! prefix-arg (+ (* 10 (or prefix-arg 0))
                                      number))
                  (message! "~A" prefix-arg))
                (loop))
               ((C-g)
                (and prefix-arg
                     (message! "(prefix-arg ~S cancelled)" prefix-arg))
                (set! prefix-arg #f)
                (loop))
               ((C-l)
                (refresh!)
                (loop))
               ((C-n)
                (skim-forw! (get-prefix-arg))
                (loop))
               ((C-p)
                (skim-back!)
                (loop))
               ((n)
                (forw! (get-prefix-arg))
                (loop))
               ((p)
                (back!)
                (loop))
               ((a)
                (let ((gi (ghost-indicator #t)))
                  (and (thunk? pos)
                       (refresh!))
                  (message! "ghost indicator: ~A" (pretty-i gi)))
                (loop))
               ((b)
                (let ((btw (btw-indicator #t)))
                  (and-let* ((pair (ov 'btw)))
                    (set-car! pair btw)
                    (refresh!))
                  (message! "btw indicator: ~A" (pretty-i btw)))
                (loop))
               ((s)
                (let ((showing (showing #t)))
                  (refresh!)
                  (message! "showing: ~A" showing))
                (loop))
               ((v)
                (let ((now (mind 'vary! (get-prefix-arg))))
                  (if (string? now)
                      (message! "(~A)" now)
                      (redraw-completely! now)))
                (loop))
               ((comma)
                (redraw-completely! (mind 'beg!))
                (message! "root")
                (loop))
               ((period)
                (let-values (((node num) (mind 'end! (get-prefix-arg))))
                  (redraw-completely! node)
                  (message! "end~A"
                            (if num
                                (fs " ~A~A"
                                    num
                                    (if (= 1 num)
                                        " -- main line"
                                        ""))
                                "")))
                (loop))
               ((slash)
                (display-metainfo!)
                (loop))
               ((C-h)
                (hey! (keybindings))
                (loop))
               (else
                (loop)))))
          ((active)
           (refresh!)
           (loop))
          ((quit)
           (bye!))
          (else
           (error "unexpected event type:" (SDL:event:type ev))))))))

(define (main/qop fop qop)

  (define (warn s . args)
    (apply simple-format (current-error-port)
           (string-append "WARNING: " s "~%")
           args))

  ((adj-c #f) showing fop)
  (adj-i btw-indicator fop)
  (adj-i ghost-indicator fop)
  (let ((filenames (qop '())))
    (or (pair? filenames)
        (error "no SGF-FILE specified, try ‘--help’"))
    (FE (cdr filenames)
        (lambda (filename)
          (warn "ignoring file: ~A" filename)))
    (let* ((filename (car filenames))
           (collection (call-with-input-file filename
                         read-sgf))
           (n (or (qop 'number string->number)
                  1)))
      (or (pair? collection)
          (error "could not read SGF-FILE" filename))
      (let* ((ocount (length collection))
             (valid (filter valid-go-game collection))
             (vcount (length valid)))
        (or (= ocount vcount)
            (warn "colletion has ~A games, but ~A valid Go games"
                  ocount (if (zero? vcount)
                             'none
                             (fs "only ~A" vcount))))
        (or (positive? vcount)
            (error "no valid Go games found in" filename))
        (or (<= 1 n vcount)
            (error (fs "invalid game number: ~A (~A)"
                       n
                       (if (= 1 vcount)
                           "only one game in the collection"
                           (fs "valid range: 1..~A" vcount)))))
        (ponder (gnugo-via-gtp)
                (make-screen fop (or (qop 'geometry)
                                     (fop 'geometry)
                                     "900x600"))
                (grok filename (list-ref valid (1- n)))
                fop))))
  (SDL:quit))

(define (main args)
  (chv args)
  (let ((qop (qop<-args
              args '((geometry (single-char #\g) (value #t))
                     (config (single-char #\c) (value #t))
                     (number (single-char #\n) (value #t))))))
    (exit (main/qop
           (fop<- (or (qop 'config)
                      'sgfv))
           qop))))

;;; sgfv.scm ends here
