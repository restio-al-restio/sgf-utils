;;; base.scm --- cerchiamo di capirci!

;; Copyright (C) 2012 Thien-Thi Nguyen
;;
;; This file is part of SGF Utils.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with SGF Utils.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Probably ‘define-macro’ is uncool for Guile 2.x.  Hmmm.

;;; Code:

(define-module (base)
  #:export (FE shunt fs))

(define-macro (FE . args)               ; snarfed from ttn-do
  (let ((proc (car (last-pair args))))
    `(for-each ,proc ,@(delq proc args))))

(define-macro (shunt proc-names . otherwise)
  ;; (put 'shunt 'scheme-indent-function 1)
  `(lambda (command . args)
     (apply (case command
              ,@(map (lambda (name)
                       `((,name) ,name))
                     proc-names)
              (else
               ,(if (null? otherwise)
                    '(error "bad command:" command)
                    `(lambda args
                       ,(car otherwise)))))
            args)))

(define (fs s . args)
  (apply simple-format #f s args))

;;; base.scm ends here
