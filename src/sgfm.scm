;;; sgfm.scm

;; Copyright (C) 2012 Thien-Thi Nguyen
;;
;; This file is part of SGF Utils.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with SGF Utils.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Usage: sgfm
;;
;; Read a collection in sexp form on stdin; write it as SGF to stdout.

;;; Code:

(define-module (sgfm)
  #:export (main))

(use-modules
 ((base) #:select (FE))
 ((bgx-banalities) #:select (chv))
 ((ff-w) #:select (write-sgf)))

(define (main args)
  (chv args)
  (write-sgf (read (current-input-port)))
  (exit #t))

;;; sgfm.scm ends here
