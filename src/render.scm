;;; render.scm --- display various things in various ways

;; Copyright (C) 2012, 2013, 2021 Thien-Thi Nguyen
;;
;; This file is part of SGF Utils.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with SGF Utils.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(define-module (render)
  #:export (make-screen)
  #:use-module (ice-9 curried-definitions))

(use-modules
 ((base) #:select (FE shunt))
 (sdl misc-utils)
 ((sdl sdl) #:prefix SDL:)
 ((sdl gfx) #:prefix GFX:)
 ((sdl ttf) #:prefix TTF:)
 ((sdl simple) #:select (simple-canvas))
 ((srfi srfi-4) #:select (s16vector))
 ((srfi srfi-11) #:select (let-values)))

(define TTF-FILENAME "/usr/share/fonts/truetype/freefont/FreeSansBold.ttf")
(define DPI 96.0)

(define (o2 n)
  (ash n -1))

(define TINT (list (cons 'B (make-list 3 #x00))
                   (cons 'W (make-list 3 #xBE))))
(define FULL (acons 'W (make-list 3 #xFF) TINT))

(define HOT-PINK #xFF69B4FF)            ; RGBA (find-file "/etc/X11/rgb.txt")

(define RED #xFF0000FF)                 ; RGBA
(define BLACK #x000000FF)               ; RGBA
(define WHITE #xFFFFFFFF)               ; RGBA

(define PI 3.14159265358979323846)

(define (w/o-alpha rgba)
  (ash rgba -8))

(define (color<-rgb-ls rgb-ls)
  (apply SDL:make-color rgb-ls))

(define rgb
  (let ((all `((black . ,(color<-rgb-ls (make-list 3 #x00)))
               (white . ,(color<-rgb-ls (make-list 3 #xFF))))))
    ;; rgb
    (lambda (name)
      (assq-ref all name))))

(define (fill-surface! surface n-rgb)
  (SDL:fill-rect surface #f n-rgb))

(define (alpha-able-surface w h)
  (SDL:make-surface w h 'src-alpha))    ; FIXME?

(define load-font
  (let ((cache '()))
    ;; load-font
    (lambda (px)
      (let ((key (cons TTF-FILENAME px)))
        (or (assoc-ref cache key)
            (let ((font (TTF:load-font TTF-FILENAME
                                       (exact-truncate
                                        ;; algebra, motherfucker:
                                        ;; do you speak it?!
                                        (* (/ 72.0
                                              DPI)
                                           px)))))
              (set! cache (acons key font cache))
              font))))))

(define (text-rendering-manager px)
  (let ((font (load-font px)))

    (define r!
      (let ((fg (rgb 'white))
            (bg (rgb 'black)))
        ;; r!
        (lambda (text)
          (SDL:display-format-alpha
           (TTF:render-utf8 font text fg bg)))))

    (define h1
      (let ((height (TTF:font:height font)))
        ;; h1
        (lambda ()
          height)))

    (shunt (h1)
      (r! command))))

(define bit-matrix
  (if (defined? 'make-typed-array)
      (lambda (col row)
        (make-typed-array 'b #t col row))
      (lambda (col row)
        (make-uniform-array #t col row))))

(define (FE-col/row cols rows proc)
  (array-index-map! (bit-matrix cols rows)
                    (lambda (col row)
                      (proc col row)
                      ;; Murphy was an optimist.
                      #t)))

(define ((FE-valid-place mask) places proc!)

  (define (unfettered col row)
    (proc! (vector col row)))

  (define (restricted col row)
    (or (array-ref mask col row)
        (unfettered col row)))

  (define (restricted-proc! v)
    (or (array-ref mask (vector-ref v 0) (vector-ref v 1))
        (proc! v)))

  (let ((lone! (if mask
                   restricted-proc!
                   proc!))
        (do-it! (if mask
                    restricted
                    unfettered)))

    (define (area! area)

      (define (v idx)
        (vector-ref area idx))

      (define (bounds lo hi)
        (list (v lo) (v hi)))

      (FE-col/row (bounds 0 2)
                  (bounds 1 3)
                  do-it!))

    (FE places (lambda (place)
                 ;; Don't do anything for ‘PASS’.
                 (or (eq? 'PASS place)
                     ((if (= 2 (vector-length place))
                          lone!
                          area!)
                      place))))))

(define (squint cols rows)
  (let* ((FE-place (FE-valid-place #f))
         (cur #f)
         (cache `((#f 0 0 ,cols ,rows #f))))

    (define (hull places)

      (define (make)
        (let ((mask (bit-matrix cols rows))
              (min-col (1- cols))
              (max-col 0)
              (min-row (1- rows))
              (max-row 0))

          (define (compare-point! col row)
            (set! min-col (min min-col col))
            (set! max-col (max max-col col))
            (set! min-row (min min-row row))
            (set! max-row (max max-row row))
            (array-set! mask #f col row))

          (define (compare! one)

            (define (v idx)
              (vector-ref one idx))

            (compare-point! (v 0) (v 1))
            (and (= 4 (vector-length one))
                 (compare-point! (v 2) (v 3))))

          (FE-place places compare!)
          (list min-col min-row
                (- max-col min-col -1)
                (- max-row min-row -1)
                mask)))

      (or (assq-ref cache places)
          (let ((m (make)))
            (set! cache (acons places m cache))
            m)))

    (define (try places)
      (let ((same? (eq? cur places))
            (m (hull places)))
        (set! cur places)
        (values same? m)))

    ;; rv
    (shunt (try))))

(define show! SDL:flip)

(define (make-screen-1 screen)
  (let ((w (screen #:w))
        (h (screen #:h))
        (to (screen))
        (cache #f)
        (vertical? #f)
        (side #f) (bg #f) (bg-pos #f)
        ;; board
        (portion #f)
        (FE-place (FE-valid-place #f))
        (unit #f) (half #f) (pp2 #f)
        (wherever-position #f)
        ;; space
        (space #f) (space-pos #f) (us #f)
        ;; etc
        (std-r! #f)
        (std-h1 #f)
        (meta-x #f)
        (meta-y-base #f)
        (comment-rect #f))

    (define (pixel-position vector)
      (pp2 (vector-ref vector 0) (vector-ref vector 1)))

    (define (compose! src pos)
      (SDL:blit-surface src #f to pos))

    (define (mini-flip! rect)
      (SDL:update-rect to rect))

    (define (zow! surface)
      (fill-surface! surface #xE7B249))

    (define (layout!)
      (set! cache '())
      (set! vertical? (< w h))
      (set! side (if vertical? w h))
      (set! bg (alpha-able-surface side side))
      (zow! bg)
      (set! bg-pos (if vertical?
                       (SDL:make-rect 0 0 w side)
                       (SDL:make-rect 0 0 side h)))
      (set! space-pos
            (apply SDL:make-rect
                   (if vertical?
                       (list 0 side w (- h side))
                       (list side 0 (- w side) h))))
      (set! space (alpha-able-surface (SDL:rect:w space-pos)
                                      (SDL:rect:h space-pos)))
      (set! us (SDL:make-rect (+ 5 (SDL:rect:x space-pos))
                              (+ 5 (SDL:rect:y space-pos))
                              (- (SDL:rect:w space-pos) 10)
                              (- (SDL:rect:h space-pos) 10)))
      (set! std-r! (text-rendering-manager (max 18 (/ side 42))))
      (set! std-h1 (std-r! 'h1))
      (let ((h (+ 4 std-h1)))
        (set! meta-x (SDL:rect:x us))
        (set! meta-y-base (+ (SDL:rect:y us)
                             (if vertical?
                                 (SDL:rect:h us)
                                 0)))))

    (define (clear!)
      (compose! bg bg-pos)
      (compose! space space-pos))

    (define (jam-unit-etc co ro cols rows mask)

      (define (but-for n)
        (+ 10 (o2 (- side 20 n))))

      (set! wherever-position (vector (+ 1 (- cols co))
                                      (ash (- rows ro) -1)))
      (zow! bg)
      (set! unit (quotient side (max cols rows)))
      (set! half (o2 unit))
      (let* ((c-m1 (1- cols))
             (r-m1 (1- rows))
             (total-w (* c-m1 unit))
             (total-h (* r-m1 unit))
             (ul-x (+ (SDL:rect:x bg-pos) (but-for total-w)))
             (ul-y (+ (SDL:rect:y bg-pos) (but-for total-h))))

        ;; Jam ‘pp2’.
        (set! pp2 (lambda (col row)
                    (values (+ ul-x (* unit (- col co)))
                            (+ ul-y (* unit (- row ro))))))

        (let ((lr-x (+ ul-x (* unit c-m1)))
              (lr-y (+ ul-y (* unit r-m1))))

          (define (line! x1 y1 x2 y2)
            (GFX:draw-thick-line bg x1 y1 x2 y2 2 #xff))

          (define (half-if expression)
            (if expression (1- half) 1))

          (define half-if-internal
            (let ((dims (and mask (array-dimensions mask))))
              ;; half-if-internal
              (lambda (get expression)
                (half-if (and dims (> (get dims)
                                      expression))))))

          (FE (iota cols)
              (let ((beg (- ul-y (half-if (positive? ro))))
                    (end (+ lr-y (half-if-internal cadr (+ ro rows)))))
                (lambda (col)
                  (let ((x (+ ul-x (* unit col))))
                    (line! x beg x end)))))
          (FE (iota rows)
              (let ((beg (- ul-x (half-if (positive? co))))
                    (end (+ lr-x (half-if-internal car (+ co cols)))))
                (lambda (row)
                  (let ((y (+ ul-y (* unit row))))
                    (line! beg y end y)))))
          (and mask
               (let ((clip (staging-rectangle))
                     (surf (staging-surface))
                     (zonk '()))
                 (fill-surface! surf (w/o-alpha BLACK))
                 (FE-col/row
                  (list co (+ co cols -1))
                  (list ro (+ ro rows -1))
                  (lambda (col row)
                    (cond ((array-ref mask col row)
                           (pixel-position/set-clip! (vector col row) clip)
                           (SDL:blit-surface surf #f bg clip)))))))))
      (set! FE-place (FE-valid-place mask))
      (clear!))

    (define (mememe! mask-places)
      (let-values (((same? m-info) (portion 'try mask-places)))
        (cond (same?)
              (else (apply jam-unit-etc m-info)
                    (set! cache '())))))

    (define (set-board-size! cols rows)
      (set! portion (squint cols rows))
      (jam-unit-etc 0 0 cols rows #f))

    (define (rgba<-color alpha color)
      (logior (ash (SDL:color:r color) 24)
              (ash (SDL:color:g color) 16)
              (ash (SDL:color:b color)  8)
              alpha))

    (define (opaque color)
      (rgba<-color #xff color))

    (define (staging-rectangle)
      (SDL:make-rect 0 0 unit unit))

    (define staging-surface
      (let ((ckey (w/o-alpha HOT-PINK)))
        ;; staging-surface
        (lambda ()
          (let ((surface (SDL:make-surface unit unit '(src-colorkey
                                                       rle-accel))))
            (fill-surface! surface ckey)
            (SDL:surface-color-key! surface ckey #t)
            surface))))

    (define (hmmm key make)
      (or (assoc-ref cache key)
          (let ((value (make)))
            (set! cache (acons key value cache))
            value)))

    (define ((repeatedly one! key make) places)
      (FE-place places (one! (hmmm key make)
                             (staging-rectangle))))

    (define (max-radius)
      ;; Stones should be "snug but not touching".
      (exact-truncate (* 0.48 unit)))

    (define (pixel-position/set-clip! where clip)
      (let-values (((x y) (pixel-position where)))
        (SDL:rect:set-x! clip (- x half))
        (SDL:rect:set-y! clip (- y half))
        (values x y)))

    (define (snap-surface clip)
      (SDL:display-format-alpha
       (copy-surface to clip)))

    (define (set-alpha-and-compose! alpha snap clip)
      (GFX:set-pixel-alpha! snap alpha)
      (compose! snap clip))

    (define (display-blurbs! ls)
      (let* ((count (length ls))
             (r! (text-rendering-manager
                  (min (/ side count 1.1)
                       (/ side 0.5 (apply max (map
                                               string-length
                                               ls))))))
             (lines (map r! ls))
             (w (+ 20 (apply max (map SDL:surface:w lines))))
             (h1 (r! 'h1))
             (h (+ 10 (* h1 count)))
             (stage (SDL:display-format-alpha
                     (SDL:make-surface w h)))
             (rect (rect<-surface stage
                                  (o2 (- side w))
                                  (o2 (- side h)))))
        (FE lines (iota count)
            (lambda (surf idx)
              (SDL:blit-surface
               surf #f stage (rect<-surface
                              surf
                              (o2 (- w (SDL:surface:w surf)))
                              (+ 5 (* idx h1))))))
        (GFX:draw-rectangle stage 1 1 (- w 2) (- h 2) RED)
        (set-alpha-and-compose! #xDD stage rect)
        (mini-flip! rect)))

    (define (nothing! places)
      (let ((clip (staging-rectangle)))

        (define (one! where)
          (pixel-position/set-clip! where clip)
          (SDL:blit-surface bg clip to clip))

        (FE-place places one!)))

    (define (stone! B/W places)
      (let* ((radius (max-radius))
             (rgb-ls (assq-ref TINT B/W))
             (color (color<-rgb-ls rgb-ls)))

        (define (pre!)
          (let ((pre (staging-surface)))

            (define (c! radius color x y)
              (GFX:draw-circle pre x y radius (opaque color) #t))

            (c! radius color half half)
            (let* ((steps (- radius 5))
                   (bump (/ 181.1 steps)))
              (let loop ((inner steps) (hi 0.0))
                (or (zero? inner)
                    (let ((slide (exact-truncate (* 0.31 (- radius inner))))
                          (more (exact-truncate hi)))
                      (c! inner (color<-rgb-ls
                                 (map (lambda (component)
                                        (min #xff (+ more component)))
                                      rgb-ls))
                          (+ half slide)
                          (- half slide))
                      (loop (1- inner) (+ bump hi))))))
            pre))

        (define ((one! pre clip) where)
          (let-values (((x y) (pixel-position/set-clip! where clip)))
            (compose! pre clip)
            (GFX:draw-aa-circle to x y radius (opaque color))))

        ((repeatedly one! (cons B/W stone!) pre!)
         places)))

    (define (stones! b-places w-places)
      (stone! 'B b-places)
      (stone! 'W w-places))

    (define (territory! B/W places)
      (let ((radius (exact-truncate (/ unit 4)))
            (rgba (opaque (color<-rgb-ls (assq-ref FULL B/W)))))

        (define (pre!)
          (let ((pre (staging-surface)))
            (GFX:draw-circle pre half half radius rgba #t)
            pre))

        (define ((one! pre clip) where)
          (pixel-position/set-clip! where clip)
          (let ((snap (snap-surface clip)))
            (SDL:blit-surface pre #f snap)
            (GFX:draw-aa-circle snap half half radius rgba)
            (set-alpha-and-compose! #x7f snap clip)))

        ((repeatedly one! (cons B/W territory!) pre!)
         places)))

    (define (territories! b-places w-places)
      (territory! 'B b-places)
      (territory! 'W w-places))

    (define (select! places)

      (define (pre!)
        (let* ((pre (staging-surface))
               (len (exact-truncate (/ unit 3)))
               (m (- half len))
               (p (+ half len))
               (ul (max 4 (exact-truncate (/ unit 42))))
               (lr (- (1- unit) ul)))

          (define (hole! x1 y1 x2 y2)
            (GFX:draw-rectangle pre x1 y1 x2 y2 HOT-PINK #t))

          (fill-surface! pre (w/o-alpha RED))
          (hole! 0 m unit p)
          (hole! m 0 p unit)
          (hole! ul ul lr lr)
          pre))

      (define ((one! pre clip) where)
        (pixel-position/set-clip! where clip)
        (compose! pre clip))

      ((repeatedly one! select! pre!)
       places))

    (define (mark! places)

      (define (pre!)
        (let* ((pre (staging-surface))
               (thickness (max 2 (exact-truncate (/ unit 19))))
               (len (exact-truncate (/ unit 4)))
               (m (- half len))
               (p (+ half len)))

          (define (line! y1 y2)

            (define (simple! x1 x2)
              (GFX:draw-line pre x1 y1 x2 y2 RED))

            (simple! m p)
            (do ((i thickness (1- i)))
                ((zero? i))
              (simple! (+ m i) (+ p i))
              (simple! (- m i) (- p i))))

          (line! m p)
          (line! p m)
          pre))

      (define ((one! pre clip) where)
        (pixel-position/set-clip! where clip)
        (compose! pre clip))

      ((repeatedly one! mark! pre!)
       places))

    (define (circle! places)
      (let* ((thickness (max 4 (exact-truncate (/ unit 10))))
             (outer (exact-truncate (/ unit 3)))
             (inner (- outer thickness)))

        (define (pre!)
          (let ((pre (staging-surface)))

            (define (simple! radius color)
              (GFX:draw-circle pre half half radius color #t))

            (simple! outer RED)
            (simple! inner HOT-PINK)
            pre))

        (define ((one! pre clip) where)
          (let-values (((x y) (pixel-position/set-clip! where clip)))

            (define (simple-aa! radius)
              (GFX:draw-aa-circle to x y radius RED))

            (compose! pre clip)
            (simple-aa! outer)
            (simple-aa! inner)))

        ((repeatedly one! circle! pre!)
         places)))

    (define ((cartesian-proc x y magnitude) angle)
      (let ((z (make-polar magnitude
                           (/ (* PI (- angle))
                              180))))
        (values (+ x (exact-truncate (real-part z)))
                (+ y (exact-truncate (imag-part z))))))

    (define (square! places)

      (define (pre!)
        (let* ((pre (staging-surface))
               (outer (1- (max-radius)))
               (inner (- outer (max 6 (exact-truncate (/ unit 10))))))

          (define (simple! magnitude color)
            (let ((cartesian (cartesian-proc half half magnitude)))
              (let-values (((ax ay) (cartesian 45))
                           ((bx by) (cartesian 135))
                           ((cx cy) (cartesian 225))
                           ((dx dy) (cartesian 315)))
                (GFX:draw-polygon pre
                                  (s16vector ax bx cx dx)
                                  (s16vector ay by cy dy)
                                  color #t))))

          (simple! outer RED)
          (simple! inner HOT-PINK)
          pre))

      (define ((one! pre clip) where)
        (pixel-position/set-clip! where clip)
        (compose! pre clip))

      ((repeatedly one! square! pre!)
       places))

    (define (triangle! places)
      (let* ((thickness (max 6 (exact-truncate (/ unit 8))))
             (outer (1- (max-radius)))
             (inner (- outer thickness)))

        (define (args x y magnitude . rest)
          (let ((cartesian (cartesian-proc x y magnitude)))
            (let-values (((ax ay) (cartesian 90))
                         ((bx by) (cartesian 210))
                         ((cx cy) (cartesian 330)))
              (cons* ax ay bx by cx cy rest))))

        (define (pre!)
          (let ((pre (staging-surface)))

            (define (simple! magnitude color)
              (apply GFX:draw-trigon pre (args half half magnitude color #t)))

            (simple! outer RED)
            (simple! inner HOT-PINK)
            pre))

        (define ((one! pre clip) where)
          (let-values (((x y) (pixel-position/set-clip! where clip)))

            (define (simple-aa! magnitude)
              (apply GFX:draw-aa-trigon to (args x y magnitude RED)))

            (compose! pre clip)
            (simple-aa! outer)
            (simple-aa! inner)))

        ((repeatedly one! triangle! pre!)
         places)))

    (define (segment! positions)

      (define (one! where)
        (let-values (((sx sy) (pixel-position (car where)))
                     ((dx dy) (pixel-position (cdr where))))
          (GFX:draw-thick-line to sx sy dx dy 3 RED)))

      (FE positions one!))

    (define (ray! positions)

      (define ((pixpos x y) z)
        (values (+ x (exact-truncate (real-part z)))
                (+ y (exact-truncate (imag-part z)))))

      (define (one! where)
        (let-values (((sx sy) (pixel-position (car where)))
                     ((dx dy) (pixel-position (cdr where))))
          (let* ((z (make-rectangular (- sx dx) (- sy dy)))
                 (m (magnitude z))
                 (a (angle z))
                 (flare (/ PI 10))
                 (close (/ unit 3))
                 (radius (max 2 (/ unit 30))))

            (define dpos (pixpos dx dy))

            (define (turned direction)
              (make-polar close (+ a (* flare direction))))

            (define (poly-vecs)

              (define spos (pixpos sx sy))

              (define zpos
                (let ((z (make-polar (* 0.9 close) a)))
                  (let-values (((zx zy) (dpos z)))
                    (pixpos zx zy))))

              (define (two-sides actually)

                (define (normal direction)
                  (make-polar radius (+ a (* (/ PI 2) direction))))

                (let-values (((mx my) (actually (normal -1)))
                             ((px py) (actually (normal  1))))
                  (values mx my px py)))

              (let-values (((x1 y1 x2 y2) (two-sides spos))
                           ((x4 y4 x3 y3) (two-sides zpos)))
                (values (s16vector x1 x2 x3 x4)
                        (s16vector y1 y2 y3 y4))))

            (let-values (((ix iy) (dpos (make-polar (/ unit 4) a)))
                         ((px py) (dpos (turned  1)))
                         ((mx my) (dpos (turned -1)))
                         ((vx vy) (poly-vecs)))
              (GFX:draw-circle to sx sy (exact-truncate radius) RED #t)
              (GFX:draw-trigon to dx dy px py mx my RED #t)
              (GFX:draw-polygon to vx vy RED #t)
              (GFX:draw-aa-circle to sx sy (exact-truncate radius) RED)
              (GFX:draw-aa-trigon to dx dy px py mx my RED)
              (GFX:draw-aa-polygon to vx vy RED)))))

      (FE positions one!))

    (define (dim! places)
      (let ((clip (staging-rectangle)))

        (define (one! where)
          (pixel-position/set-clip! where clip)
          (let ((snap (snap-surface clip)))
            (SDL:blit-surface bg clip to clip)
            (set-alpha-and-compose! #x7f snap clip)))

        (FE-place places one!)))

    (define (one-label! pinstripe-color)
      (define r! (text-rendering-manager (* 0.75 unit)))
      ;; rv
      (lambda (where text)
        ;; If ‘where’ is symbol ‘PASS’, arrange to place the label
        ;; in space and annotate it w/ "(PASS)" as well.
        (case where
          ((PASS)
           (set! text (string-append text " (PASS)"))
           (set! where wherever-position)))
        (let* ((label (r! (string-append " " text " ")))
               (w (SDL:surface:w label))
               (h (r! 'h1)))
          (GFX:draw-rectangle label 1 1 (- w 2) (- h 2) pinstripe-color)
          (set-alpha-and-compose! #xDD label
                                  (let-values (((x y) (pixel-position where)))
                                    (rect<-surface label
                                                   (- x (o2 w))
                                                   (- y (o2 h))))))))

    (define (multi-future-label! positions)
      (FE positions
          (map number->string (map 1+ (iota (length positions))))
          (one-label! WHITE)))

    (define (label! specs)
      (FE (map car specs)
          (map cdr specs)
          (one-label! RED)))

    (define (display-meta! pos ls)
      (and (thunk? pos)
           (let-values (((ghosts who see me) (pos)))
             (cond (see (stone! who ghosts)
                        (I see ghosts)
                        (dim! ghosts)))
             (set! pos me)))
      (and (vector? pos)
           (select! (list pos)))
      (let ((count (length ls)))
        (FE (iota count)
            ls
            (lambda (idx text)
              (let* ((surf (std-r! text))
                     (rect (rect<-surface surf meta-x
                                          (+ meta-y-base
                                             (* idx std-h1)
                                             (if vertical?
                                                 (- (* count std-h1))
                                                 0)))))
                (compose! surf rect))))))

    (define (comment! ls)

      (define (pre!)

        (define raw-r! (text-rendering-manager (max 20 (/ unit 10))))

        (define (r! string)
          (raw-r! (string-append " " string)))

        (let* ((h1 (raw-r! 'h1))
               (showing 0)
               (surf #f))

          (define (prep! line-count y)
            (let ((copy (and (positive? y)
                             (copy-surface surf))))
              (cond ((< showing line-count)
                     (set! showing line-count)
                     (set! surf (SDL:make-surface
                                 (if vertical?
                                     (exact-truncate
                                      (* 0.75 (SDL:surface:w space)))
                                     (SDL:rect:w us))
                                 (+ 4 (* showing h1))))
                     (set! comment-rect
                           (rect<-surface
                            surf (+ (SDL:rect:x us)
                                    (if vertical?
                                        (- (SDL:rect:w us)
                                           (SDL:surface:w surf))
                                        0))
                            (+ (SDL:rect:y us)
                               (SDL:rect:h us)
                               (* showing (- h1))
                               -4)))))
              (fill-surface! surf 0)
              (and copy (SDL:blit-surface copy #f surf #f))))

          (define (finish!)
            (GFX:draw-vline surf 1 1 h1 RED)
            (GFX:draw-hline surf 1 h1 1 RED)
            (compose! surf comment-rect))

          (define (cut string avail-w w)
            (let ((first-guess (exact-truncate
                                (* (string-length string)
                                   (/ avail-w w)
                                   0.941))))
              (let loop ((i first-guess))
                (cond ((zero? i) first-guess)
                      ((char=? #\space (string-ref string i)) i)
                      (else (loop (1- i)))))))

          (define (fill! ls)
            (let* ((count (length ls))
                   (avail-w (begin (prep! count 0)
                                   (- (SDL:surface:w surf) 4)))
                   (at (SDL:make-rect 2 0 0 h1)))
              (let loop ((y 2) (ls ls))
                (if (pair? ls)
                    (let* ((string (car ls))
                           (ln (r! string))
                           (w (SDL:surface:w ln))
                           (rest (cdr ls))
                           (next-y (+ y h1))
                           (tail #f))
                      (and (< avail-w w)
                           (let ((pos (cut string avail-w w)))
                             (set! ln (r! (substring string 0 pos)))
                             (set! w (SDL:surface:w ln))
                             (set! rest (cons (substring string pos)
                                              rest))))
                      (SDL:rect:set-y! at y)
                      (SDL:rect:set-w! at w)
                      (SDL:blit-surface ln #f surf at)
                      (prep! (+ (/ (- next-y 2) h1)
                                (length rest))
                             next-y)
                      (loop next-y rest))
                    (finish!)))))

          fill!))

      ((hmmm comment! pre!)
       ls))

    (define (message! text)

      (define (pre!)
        (let ((rect (SDL:make-rect (SDL:rect:x us) -1
                                   (SDL:rect:w us) std-h1))
              (bg (SDL:map-rgb (SDL:surface-get-format to)
                               ;; TODO: std-bg
                               (rgb 'black))))

          (define (spew! text)
            (let ((msg (std-r! text)))
              (SDL:rect:set-y! rect (- (if comment-rect
                                           (SDL:rect:y comment-rect)
                                           (+ (SDL:rect:y us)
                                              (SDL:rect:h us)))
                                       std-h1 4))
              (SDL:fill-rect to rect bg)
              (mini-flip! rect)
              (set-alpha-and-compose! #xFF msg rect)
              (mini-flip! rect)))

          spew!))

      ((hmmm message! pre!)
       text))

    (define I (shunt (clear!
                      mememe!
                      display-blurbs!
                      nothing!
                      stones! territories!
                      select! mark! circle! square! triangle!
                      segment! ray!
                      dim!
                      multi-future-label! label!
                      display-meta!
                      comment! message!
                      set-board-size!
                      show!)))

    (layout!)
    (clear!)
    ;; rv
    I))

(define (make-screen fop geometry)
  (TTF:ttf-init)
  (and=> (fop 'font) (lambda (filename)
                       (set! TTF-FILENAME filename)))
  (and=> (fop 'dpi) (lambda (dpi)
                      (set! DPI dpi)))
  (let ((rect (rectangle<-geometry-string geometry)))
    (make-screen-1 (simple-canvas #t
                                  (SDL:rect:w rect)
                                  (SDL:rect:h rect)
                                  32))))

;;; render.scm ends here
