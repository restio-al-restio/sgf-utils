(;FF[4]GM[1]
  C[This is an example SGF file, to demonstrate (and test) \
      the features of sgfv, the viewing utility. \
      Type 'n' to proceed.]
  GN[a gnarly example]
  DT[@EG_DT@]
  PC[@EG_PC@ (right here!)]
  EV[sgfv @PACKAGE_VERSION@ demo]
  PB[zaphod]
  PW[beeblebrox]
  SO[Imagination & Hubris]
  CP[2012 Thien-Thi Nguyen]
  SZ[11]

;B[ee]N[First move!]
 C[You should see a black stone on the board, \
     and the move number (1), player B and position \
     in the off-board space (henceforth simply "space").

   Type 'p' to revisit the root node, \
     'n' again to return here, and \
     'C-h' (control h) to summarize the key bindings.]

;W[jb]N[Second move!]
 C[Note that the move column is K and the move row is 10; \
     column order is left to right, row order is bottom to top.]

;B[ej]N[Third move!]
 SQ[ch:gk]
 CR[aa][bb][ca][db]
 TR[jh][ki][jj][kk]
 SL[ag][bf][ce][dd]
 MA[ga][fb][ec][dd]
 TB[gc:ie]
 TW[ie:kg]
 C[You should also see various symbols: \
     squares, circles, triangles, "selection" \
     (four brackets in the corners), \
     marks (an X), and territories (both black and white). \
     There are two positions where symbols overlap \
     (this file is not strictly SGF compliant). \
     Can you find them?

   You can cycle through the three different "showing" \
     modes by typing 's' (try it!).]

;W[ii]N[Fourth move!]
 LN[dd:cc][cc:bd][bd:be][be:cf][cf:df][df:de]
 AR[ce:ee][ee:fc][ee:fg][ee:hd][ee:hf]
 LB[hf:zow!][jb:2][ej:?][ii:fourth move]
 TR[dh:ik]
 DD[ei:hj]
 C[This shows line segments, arrows, labels and "dimming". \
     (If you don't see them, type 's' until you do.)

   Note that each end-point or label center lies on a board position, \
     and that labels "cover" everything (although they are slightly \
     transparent).]

;B[af]N[5th move!]
 DO[]
 TR[dh:ik]
 C[How strange! \
     (Wouldn't B6 or C9 be better?)

   Well, no matter. \
     Here, the feature is the "btw" indicator. \
     You can type 'b' to cycle through the choices.

   Note that dimming is still in effect.]

;W[cc]N[6th move!]
 TR[dh:ik]
 DD[]
 C[The marked positions are variations (sub-game trees) \
     that begin with the next move.  If you type in a number \
     such as '2' or '3' before typing 'n', play will proceed \
     with the specified variation.  Type 'C-g' to cancel this \
     "prefix arg".

   If the prefix arg is too large, such as 4, it is taken \
     to be the highest-numbered variation (in this example, 3).

   Dimming, present since the fourth move, is now cancelled.]

(;B[cd]N[7th move, first in v.1]
  C[Note the variation lineage "v.1". \
      The path through variations with only "1" in the lineage \
      is also called the "main line". \
      The "/3" means there are three sibling variations, \
      two others besides the current one. \
      Type 'v' to jump to v.2, or '3v' to jump to v.3.]

(;W[dc]N[8th move, first in v.1.1]
  C[More on the main line...

    Type 'v' to jump to v.1.2.]

 ;B[hd]N[9th move, v.1.1]
  C[Note that the lineage has no "/N"; there are no variations \
      (type 'v' to check).]

 ;W[]  N[10th move, v.1.1] BM[2] HO[2] GB[2]
 ;B[hg]N[11th move, v.1.1]
 ;W[]  N[12th move, v.1.1]
 ;VW[bb:dd][gb:ij][ak][ka]
  AR[hd:cd][dc:ka]
  LN[ai:ag][ag:bh][bh:cg][cg:ci][dg:dh][dh:ei][ei:ii][ii:jh][jh:jg]
  LB[gi:(oop ack)]
  C[Concavity!
    Depravity!
    (This and the following nodes are move-less; \
      their purpose is to play around with the \
      "partial view" feature. \
      Note that arrows and line segments are never hidden. \
      Labels attached to a visible position are likewise unrestricted. \
      By the way, A1 should be indicated, but not A2.)]
 ;VW[]C[Ready?]
 ;VW[aa:kj]C[Clip bottom:]
 ;VW[aa:ki]C[some more...]
 ;VW[aa:kh]C[more up...]
 ;VW[aa:kg]C[and up...]
 ;VW[aa:kf]C[until...]
 ;VW[aa:ke]C[only five rows remain.]
 ;VW[aa:je]C[Now start clipping right side:]
 ;VW[aa:he]C[more left...]
 ;VW[aa:ge]C[and left...]
 ;VW[aa:fe]C[until...]
 ;VW[aa:ee]C[only five columns remain.]
 ;VW[ab:ef]C[Now keep the clip area constant but move the "view port":]
 ;VW[ac:eg]C[more down...]
 ;VW[ad:eh]C[and down...]
 ;VW[ae:ei]C[...]
 ;VW[af:ej]C[until...]
 ;VW[ag:ek]C[we are at the bottom left corner.]
 ;VW[bf:fj]C[Now move right and up...]
 ;VW[cg:gk]C[right and down...]
 ;VW[df:hj]C[zig...]
 ;VW[eg:ik]C[zag...]
 ;VW[ff:jj]C[zig...]
 ;VW[gg:kk]C[zag, until we are at the bottom right corner.]
 ;VW[ff:kk]C[Now expand a bit.]
 ;VW[ee:kk]C[... more ...]
 ;VW[dd:kk]C[... more ...]
 ;VW[cc:kk]C[... more ...]
 ;VW[bb:kk]C[... more ...]
 ;VW[]     C[OK, done playing around.  Full view restored.]

 ;B[]  N[13th move, v.1.1]
       C[This is the first "end" (and the only one on the main line). \
           You can type 'f' now to see the final territories. \
           This may take a while, if your computer is as old (slow) \
           as the author's. :-D]
)

(;W[ec]MN[42]N["8th" move, first in v.1.2]
  C[Note that the SGF file has changed the move number, \
      just because it can.  What folly!

   Type 'v' to jump to v.1.1.]

 ;B[hd]N["9th" / 43rd move, v.1.2]
 ;W[]  N["10th" / 44th move, v.1.2]
 ;B[]  N["11th" / 45th move, v.1.2]
       C[This is the second "end".]
)
)

(;B[bd]N[7th move, v.2]
  C[Type 'v' to jump to v.3, or '1v' to jump to v.1.]

 ;W[dc]N[8th move, v.2]
)

(;B[he]N[7th move, v.3]
  C[Type 'v' to jump to v.1, or '2v' to jump to v.2.]

 ;W[dc]N[8th move, v.3]
)

)
