;;; sgfc.scm

;; Copyright (C) 2012 Thien-Thi Nguyen
;;
;; This file is part of SGF Utils.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with SGF Utils.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Usage: sgfc [options] [FILE...]
;;
;; Check well-formedness and style of FILE... (stdin if FILE is "-").
;; Display "FILE: OK" if ok, otherwise diagnostics, to stderr.
;;
;; Options:
;;
;;  -x, --sexp        -- pretty-print accumulated collection
;;                       to stdout after checking everything

;;; Code:

(define-module (sgfc)
  #:export (main)
  #:autoload (srfi srfi-1) (append-map)
  #:autoload (ice-9 pretty-print) (pretty-print))

(use-modules
 ((base) #:select (FE))
 ((bgx-banalities) #:select (chv qop<-args))
 ((ff-r) #:select (read-sgf)))

(define fse
  (let ((cep (current-error-port)))
    ;; fse
    (lambda (s . args)
      (apply simple-format cep s args))))

(define (check filename)
  (let ((port (if (string=? "-" filename)
                  (current-input-port)
                  (open-input-file filename))))
    (let ((collection (read-sgf port)))
      (fse "~A: OK~%" (port-filename port))
      (close-port port)
      collection)))

(define (main args)
  (chv args)
  (let* ((qop (qop<-args args '((sexp (single-char #\x)))))
         (input (qop '())))
    (if (qop 'sexp)
        (pretty-print (append-map check input))
        (FE input check)))
  (exit #t))

;;; sgfc.scm ends here
