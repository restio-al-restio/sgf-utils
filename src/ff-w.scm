;;; ff-w.scm --- SGF FF[4] writing

;; Copyright (C) 2012, 2014 Thien-Thi Nguyen
;;
;; This file is part of SGF Utils.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with SGF Utils.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Adapted from ttn-do 406 sgfc.scm.

;;; Code:

(define-module (ff-w)
  #:export (write-sgf)
  #:use-module (ice-9 curried-definitions))

(use-modules
 ((base) #:select (FE))
 ((ff-data) #:select (property-specs))
 ((srfi srfi-13) #:select (substring/shared
                           string-index
                           string-concatenate-reverse))
 ((srfi srfi-14) #:select (char-set
                           char-set-adjoin)))

(define sub substring/shared)

(define a-int (char->integer #\a))

(define (board-letter<-int n)
  (integer->char (+ n a-int)))

(define (compute-wproc prop)

  (define ((base-type-stringifier composition?) symbol)

    (define escape
      (let* ((bad (char-set #\] #\\))
             (ugh (if (and (pair? composition?)
                           (car composition?))
                      (char-set-adjoin bad #\:)
                      bad)))
        ;; escape
        (lambda (s)
          (let loop ((start 0) (acc '()))
            (cond ((string-index s ugh start)
                   => (lambda (pos)
                        (loop (1+ pos)
                              (cons* (string #\\ (string-ref s pos))
                                     (sub s start pos)
                                     acc))))
                  ((zero? start)
                   s)
                  (else
                   (string-concatenate-reverse acc (sub s start))))))))

    (define (unvec v)
      (define (l<- idx)
        (board-letter<-int (vector-ref v idx)))
      (if v (let ((col (l<- 0))
                  (row (l<- 1)))
              (if (= 4 (vector-length v))
                  (string col row #\:
                          (l<- 2)
                          (l<- 3))
                  (string col row)))
          ""))

    (case symbol
      ((simpletext text) escape)
      ((color) (lambda (symbol)
                 (string-capitalize (symbol->string symbol))))
      ((move) (lambda (v)
                (if (eq? 'PASS v)
                    ""
                    (unvec v))))
      ((stone point) unvec)
      ((real double number) (lambda (n)
                              (cond ((number? n) n)
                                    ((eq? #f n) "-")
                                    (else (error "bad number:" n)))))
      ((none) (lambda ignored ""))
      (else (error "badness!:" symbol))))

  (define (wproc<-base x . composition?)

    (define b-t-s (base-type-stringifier (and (pair? composition?)
                                              (car composition?))))

    (cond ((symbol? x)
           (let ((s (b-t-s x)))
             (lambda (val)
               (display (s val)))))
          ((vector? x)
           (let ((s (b-t-s (vector-ref x 0))))
             (lambda (val)
               (display (s val)))))
          (else
           (error "badness!:" (list x prop)))))

  (define (wproc<-composition x)

    (define (proc sel)
      (wproc<-base (sel x) #t))

    (let ((one (proc car))
          (two (proc cdr)))
      (lambda (v)
        (one (car v))
        (display ":")
        (two (cdr v)))))

  (define (wproc<- x)
    (if (pair? x)
        (wproc<-composition x)
        (wproc<-base x)))

  (let* ((full-spec (property-specs prop))
         (ls<- (if (memq (car full-spec) '(list elist))
                   (lambda (wproc)
                     (lambda (val)
                       (FE val wproc)))
                   identity))
         (single (if (eq? identity ls<-)
                     (car full-spec)
                     (cadr full-spec)))
         (choice? (and (pair? single)
                       (pair? (cdr single))
                       (< 2 (length single))
                       (eq? 'or (car single))))
         ;; for write, ‘optional?’ does not make sense, however the upshot of
         ;; that case is to use ‘w-fallback’ anyway, so we need check neither
         ;; for nor against it
         (w-fallback (wproc<- (if choice?
                                  (caddr single)
                                  single))))

    (define (brace wproc)
      (lambda (x)
        (display "[")
        (wproc x)
        (display "]")))

    (ls<- (brace (if choice?
                     ;; anyway, we should keep in mind this distillation of
                     ;; cases falls out of the ‘(or FOO (FOO . BAR))’
                     ;; restriction; the following mapping of a cons cell to
                     ;; ‘(FOO . BAR)’ needs to be updated if that restriction
                     ;; changes
                     (let ((w-one (wproc<-base (cadr single))))
                       (lambda (x)
                         (cond ((pair? x)
                                (w-one (car x))
                                (display ":")
                                (w-fallback (cdr x)))
                               (else
                                (w-one x)))))
                     w-fallback)))))

(define property-writer
  (let ((ht (make-hash-table (1+ 42))))
    ;; rv
    (lambda (prop)
      (or (hashq-ref ht prop)
          (let ((v (compute-wproc prop)))
            (hashq-set! ht prop v)
            v)))))

;; Write the @var{collection} of game trees to the current output port.
;;
(define (write-sgf collection)

  (define (display-gametree gametree)

    (define (display-property x)
      (let ((name (car x)))
        (display name)
        ((property-writer name) (cdr x))))

    (define (display-node x)
      (display ";")
      (FE x display-property)
      (and (pair? x) (pair? (cdr x)) (newline)))

    (display "(")
    (FE (car gametree) display-node)
    (FE (cdr gametree) display-gametree)
    (display ")")
    (newline))

  ;; do it!
  (FE collection display-gametree))

;;; ff-w.scm ends here
