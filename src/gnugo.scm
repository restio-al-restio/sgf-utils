;;; gnugo.scm --- GNU Go backend via Go Text Protocol

;; Copyright (C) 2012, 2021 Thien-Thi Nguyen
;;
;; This file is part of SGF Utils.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with SGF Utils.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; A gnugo subprocess is encapsulated by a GTP (Go Text Protocol) connection,
;; created by calling ‘make-gnugo-gtp-connection’, which returns a closure
;; that accepts the command:
;;
;;  DISPOSITION GTP-COMMAND [ARGS ...]
;;
;; GTP-COMMAND is a symbol, such as ‘help’.  ARGS are strings or symbols.
;; DISPOSITION controls how the subprocess response (a list of strings) is
;; further processed:
;;
;; * #f
;;   Discard output; return ‘#f’.
;;
;; * ()
;;   The empty list means to join the strings w/ ‘#\space’ and then
;;   tokenize them, discarding whitespace.
;;
;; * #\space
;; * #\newline
;;   Join the strings w/ the specified character, returning a single string.
;;
;; If DISPOSITION is not one listed above, return the raw list of strings.
;;
;; The "gnugo" subprocess is started with args: "--mode gtp --quiet".
;; Additional args (strings) can be passed to ‘make-gnugo-gtp-connection’.

;;; Code:

(define-module (gnugo)
  #:export (gnugo-via-gtp))

(use-modules
 ((base) #:select (shunt fs))
 ((srfi srfi-13) #:select (string-join
                           string-tokenize
                           substring/shared))
 ((ice-9 popen) #:select (open-input-output-pipe))
 ((ice-9 rdelim) #:select (read-line)))

(define A-int (char->integer #\A))

(define (w/o-2 string)
  (substring/shared string 2))

(define (make-gnugo-gtp-connection)
  (let ((child (open-input-output-pipe "gnugo --mode gtp --quiet")))

    (define (send/reply command args)
      (display (string-join (map (lambda (x)
                                   (fs "~A" x))
                                 (cons command args)))
               child)
      (newline child)
      (force-output child)
      (let loop ((acc '()))
        (let ((line (read-line child)))
          (cond ((eof-object? line)
                 (error "protocol error"))
                ((and (string-null? line)
                      (pair? acc))
                 (reverse! acc))        ; rv
                (else
                 (loop (cons line acc)))))))

    (define (query command args)      ; => (LINE ...)
      (let ((reply (send/reply command args)))
        (if (pair? reply)
            (if (< 2 (string-length (car reply)))
                (cons (w/o-2 (car reply))
                      (cdr reply))
                (cdr reply))
            reply)))

    (define s<-
      (let ((as-strings (map (lambda (c)
                               (cons c (string c)))
                             '(#\space #\newline))))
        ;; s<-
        (lambda (delim ls)
          (string-join ls (assq-ref as-strings delim)))))

    ;; rv
    (lambda (disposition command . args)
      (let ((raw (query command args)))
        (and disposition
             (case disposition
               ((()) (string-tokenize (s<- #\space raw)))
               ((#\space #\newline) (s<- disposition raw))
               ((number) (string->number (car raw)))
               (else raw)))))))

(define (gnugo-via-gtp)
  (let ((conn (make-gnugo-gtp-connection))
        (sz #f))

    (define (cq . args)
      (apply conn #f args))

    (define (set-board-size! size)
      (set! sz size)
      (cq 'boardsize size))

    (define (inverted-y n)
      (- sz 1 n))

    (define (cc<-position position)
      (fs "~A~A"
          (let ((col (vector-ref position 0)))
            (integer->char (+ A-int col (if (< 7 col)
                                            1
                                            0))))
          (1+ (inverted-y (vector-ref position 1)))))

    (define (cc/PASS<-position position)
      (if (vector? position)
          (cc<-position position)
          'PASS))

    (define (play color position)
      (cq 'play color (cc/PASS<-position position)))

    (define (position<-cc cc)
      (vector (let ((col (- (char->integer (string-ref cc 0))
                            A-int)))
                (- col (if (< 7 col)
                           1
                           0)))
              (inverted-y (1- (string->number (substring/shared cc 1))))))

    (define (lop command . args)        ; list of positions
      (map position<-cc (apply conn '() command args)))

    (define (get-positions color)
      (lop 'list_stones color))

    (define (final-info)

      (define (cap who)
        (conn 'number 'captures who))

      (define (fl kind)                 ; "final-info lop"
        (lop 'final_status_list kind))

      (values (conn #\space 'final_score)
              (cap 'B)
              (cap 'W)
              (fl 'black_territory)
              (fl 'white_territory)
              (fl 'dead)
              (fl 'seki)))

    ;; rv
    (shunt (set-board-size!
            position<-cc
            cc/PASS<-position
            play
            get-positions
            final-info)
      (apply cq command args))))

;;; gnugo.scm ends here
