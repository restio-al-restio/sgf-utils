;;; ff-r.scm --- SGF FF[4] reading

;; Copyright (C) 2012, 2014 Thien-Thi Nguyen
;;
;; This file is part of SGF Utils.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with SGF Utils.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Adapted from ttn-do 406 sgfc.scm.

;;; Code:

(define-module (ff-r)
  #:export (read-sgf
            nna
            children
            nodes
            node-move
            get-one))

(use-modules
 ((base) #:select (FE fs))
 ((ff-data) #:select (property-specs))
 ((srfi srfi-11) #:select (let-values))
 ((srfi srfi-13) #:select ((substring/shared . sub)
                           reverse-list->string
                           string-index
                           string-concatenate-reverse
                           string-join
                           string-trim-both
                           string-tokenize))
 ((srfi srfi-14) #:select (char-set-complement
                           char-set:blank
                           char-set-contains?))
 ((ice-9 rdelim) #:select (read-line))
 ((ice-9 rw) #:select (read-string!/partial)))

;; Return a string made from reading bytes until EOF from @var{port}.
;;
(define (drain port)

  (define (quickly)
    (let* ((len (- (stat:size (stat port))
                   (ftell port)))
           (buf (make-string len)))
      (let loop ((start 0))
        (if (< start len)
            (loop (+ start (read-string!/partial buf port start)))
            buf))))

  (define (slowly)
    (let loop ((acc '()))
      (let ((c (read-char port)))
        (if (eof-object? c)
            (reverse-list->string acc)
            (loop (cons c acc))))))

  ((if (false-if-exception (file-port? port))
       quickly
       slowly)))

;; Scan @var{port} until encountering @code{(;} (open-paren, semicolon)
;; at beginning of line (ignoring leading horizontal whitespace), or EOF.
;; Return @var{port}.
;;
(define (ignoring-until-bol-open-paren-semicolon port)
  (let loop ()

    (define (skip-line!)
      (read-line port)                  ; discard
      (loop))

    (let ((c (peek-char port)))
      (cond ((eof-object? c))
            ((char-set-contains? char-set:blank c)
             (read-char port)           ; discard
             (loop))
            ((not (char=? #\( c))
             (skip-line!))
            (else
             (let ((open-paren (read-char port)))
               (if (char=? #\; (peek-char port))
                   ;; done
                   (unread-char open-paren port)
                   ;; not done
                   (skip-line!)))))))
  port)

;;; TODO: Expose this to documentation extractors.

;; @var{collection} is a list of one or more game trees.  A game tree is
;; a pair whose CAR is a list of one or more nodes, and whose CDR is
;; zero or more children game trees.  A node is a list of pairs, the
;; car of which is a keyword representing an SGF property and the cdr
;; the associated value, typically a string, number, two-element vector,
;; or a pair of one of these simpler types.  A pair for the property
;; value indicates a @dfn{composed type}.
;;
;; The vector elements are integers specifying column and row zero-based
;; coordinates on a board.  For example, @code{#(4 2)} represents
;; position @code{E3}.  For board size @var{n}, the vector
;; @code{#(@var{n} @var{n})} means ``PASS''.
;; As a special case, a move expressed as a the empty string
;; (e.g., @samp{W[]}) has an internal value of @code{PASS} (a symbol).

(define a-int (char->integer #\a))

(define (int<-board-letter c)
  (- (char->integer c) a-int))

(define (happy! . whatever) #t)

(define (is-colon? c) (char=? #\: c))
(define (is-close? c) (char=? #\] c))

;; SGF grammar:
;;
;; Collection = GameTree { GameTree }
;; GameTree   = "(" Sequence { GameTree } ")"
;; Sequence   = Node { Node }
;; Node       = ";" { Property }
;; Property   = PropIdent PropValue { PropValue }
;; PropIdent  = UcLetter { UcLetter }
;; PropValue  = "[" CValueType "]"
;; CValueType = (ValueType | Compose)
;; ValueType  = (None | Number | Real | Double | Color | SimpleText |
;;               Text | Point  | Move | Stone)
;;
;; The above grammar has a number of simple properties which enables us
;; to write a simpler parser:
;;   1) There is never a need for backtracking
;;   2) The only recursion is on gametree.
;;   3) Tokens are only one character

(define tt-means-PASS?
  ;; For FF[3], a ‘PASS’ may be represented as "tt" if the board size
  ;; is 19x19 or less, rather than the empty value "" as per FF[4].
  ;; We could pass this information down to the leaf-level reading procs,
  ;; but it's much cleaner to use a fluid for that.
  (make-fluid))

(define (compute-rproc prop)

  (define (base-type-objectifier symbol)

    (define (unescape string)
      (let loop ((start 0) (acc '()))
        (cond ((string-index string #\\ start)
               => (lambda (pos)
                    (let ((c (string-ref string (1+ pos)))
                          (ok (cons (sub string start pos) acc))
                          (more (+ 2 pos)))
                      (case c
                        ((#\newline #\cr)
                         ;; Consume backslash at "EOL", which may consist
                         ;; of one the sequences: LF, CR, LFCR and CRLF.
                         (and (case (and (< more (string-length string))
                                         (string-ref string more))
                                ((#\newline) (char=? #\cr c))
                                ((#\cr) (char=? #\newline c))
                                (else #f))
                              (set! more (1+ more)))
                         (loop more ok))
                        (else
                         ;; Convert backslash-foo to simply foo.
                         (loop more (cons (string c) ok)))))))
              ((zero? start)
               string)
              (else
               (string-concatenate-reverse acc (sub string start))))))

    (define squeeze-horizontal-whitespace
      (let ((cs (char-set-complement char-set:blank)))
        ;; squeeze-horizontal-whitespace
        (lambda (string)
          (string-trim-both
           (string-join
            (string-tokenize string cs))))))

    (define (normalize-string string)
      (squeeze-horizontal-whitespace (unescape string)))

    (define (vec s)
      (define (n<- idx)
        (int<-board-letter (string-ref s idx)))
      (and (not (string-null? s))       ; allow ‘DD[]’ for example
           (let ((col (n<- 0))
                 (row (n<- 1)))
             (cond ((string-index s #\:)
                    ;; area
                    => (lambda (colon)
                         (vector
                          ;; upper left
                          col row
                          ;; lower right
                          (n<- (+ colon 1))
                          (n<- (+ colon 2)))))
                   (else
                    ;; single
                    (vector col row))))))

    (case symbol
      ((simpletext text) normalize-string)
      ((color) (lambda (s)
                 (string->symbol (string-downcase s))))
      ((stone point) vec)
      ((move) (lambda (s)
                (if (or (string-null? s)
                        (and (string=? "tt" s)
                             (fluid-ref tt-means-PASS?)))
                    'PASS
                    (vec s))))
      ((real number) string->number)
      ((double) (lambda (s)
                  (cond ((string=? "1" s) 1)
                        ((string=? "2" s) 2)
                        (else (error (fs "bad value (not 1 or 2): ~A" s))))))
      ((none) (lambda (s)
                (or (string-null? s)
                    (error (fs "non-empty value: ~S" s)))))
      (else (error "badness!:" symbol))))

  (define (rproc<-base x f?)
    (cond ((symbol? x)
           (let ((o (base-type-objectifier x)))
             (lambda (n? r!)
               (or (n?) (error "expecting value:" (list prop x)))
               (o (r! f?)))))
          ((vector? x)
           (let ((o (base-type-objectifier (vector-ref x 0)))
                 (ranges (cdr (vector->list x))))
             (lambda (n? r!)
               (or (n?) (error "expecting value:" (list prop x)))
               (let ((v (o (r! f?)))
                     (ok? #f))
                 (FE ranges (lambda (range)
                              (set! ok? (or ok? (<= (car range)
                                                    v
                                                    (cdr range))))))
                 (or ok? (error "out of range:" v))
                 v))))
          (else
           (error "badness!:" (list x f? prop)))))

  (define (rproc<-composition x)
    (let ((one (rproc<-base (car x) is-colon?))
          (two (rproc<-base (cdr x) is-close?)))
      (lambda (n? r!)
        (or (n?) (error "expecting value:" prop))
        (cons (one happy! r!) (two happy! r!)))))

  (define (rproc<- x f?)
    (if (pair? x)
        (rproc<-composition x)
        (rproc<-base x f?)))

  (define (particulars prop)            ; => values: single / ls<-
    (let ((full-spec (property-specs prop)))
      (case (car full-spec)
        ((list elist)
         (values (cadr full-spec)
                 (lambda (rproc)
                   (lambda (n? r!)
                     (let loop ((acc '()))
                       (if (n?)
                           (loop (cons (rproc happy! r!)
                                       acc))
                           (reverse! acc)))))))
        (else
         (values (car full-spec)
                 identity)))))

  ;; do it!
  (let-values (((single ls<-) (particulars prop)))
    (let* ((choice? (and (pair? single)
                         (pair? (cdr single))
                         (< 2 (length single))
                         (eq? 'or (car single))))
           (firstpick (and choice? (cadr single)))
           (optional? (eq? 'none firstpick))
           (fallback (if choice?
                         (caddr single)
                         single))
           (r-fallback (rproc<- fallback is-close?)))
      (ls<- (cond (optional?
                   (lambda (n? r!)
                     (and (n?) (r-fallback happy! r!))))
                  (choice?
                   ;; we only handle ‘(or FOO (FOO . BAR))’ because
                   ;; otherwise elaborate backtracking is required
                   (or (and (symbol? firstpick)
                            (pair? fallback)
                            (eq? firstpick (car fallback)))
                       (error "spec requires backtracking!:"
                              (assq prop *properties*)))
                   (let* ((box (list #f))
                          (f? (lambda (c)
                                (and (or (is-colon? c)
                                         (is-close? c))
                                     (begin
                                       (set-car! box c)
                                       #t))))
                          (r-one (rproc<-base firstpick f?)))
                     (lambda (n? r!)
                       (set-car! box #f)
                       (let ((v1 (r-one n? r!)))
                         (if (is-close? (car box))
                             v1
                             (let ((v2 (r-fallback happy! r!)))
                               (cons v1 v2)))))))
                  (else
                   r-fallback))))))

(define property-reader
  (let ((ht (make-hash-table (1+ 42))))
    ;; rv
    (lambda (prop)
      (or (hashq-ref ht prop)
          (let ((v (compute-rproc prop)))
            (hashq-set! ht prop v)
            v)))))

;; Return the collection of game trees parsed from reading @var{port}.
;;
(define (read-sgf port)
  (let* ((s (drain (ignoring-until-bol-open-paren-semicolon port)))
         (len (string-length s))
         (pos 0))

    (define (bump!)
      (set! pos (1+ pos)))

    (define (at position)
      (string-ref s position))

    (define (sw position)
      (cond ((= len position) position)
            ((char-whitespace? (at position)) (sw (1+ position)))
            (else position)))

    (define (sw!)
      (set! pos (sw pos)))

    (define (<> start end)
      (sub s start end))

    (define (<!> start end)
      (set! pos end)
      (sub s start end))

    (define (ncbv?)                     ; next char bears value?
      (sw!)
      (if (= len pos)
          #f
          (char=? #\[ (at pos))))

    (define (check-too-short!)
      (or (> len pos)
          (error "unexpected EOF")))

    (define (s1rt finish?)              ; skip 1 read 'til
      (bump!)
      (check-too-short!)
      (let ((start pos))
        (let loop ((end start))
          (if (and (finish? (at end))
                   ;; don't be fooled by backslash escaping
                   (not (char=? #\\ (at (1- end)))))
              (begin
                (set! pos (if (is-close? (at end))
                              (1+ end)
                              end))
                (<> start end))
              (loop (1+ end))))))

    (define (read-property)
      (sw!)
      (and (char<=? #\A (at pos) #\Z)
           (let* ((c1 (at (1+ pos)))
                  (plen (if (char<=? #\A c1 #\Z) 2 1))
                  (prop (string->symbol (<!> pos (+ pos plen))))
                  (reader (property-reader prop))
                  (value (reader ncbv? s1rt)))
             (cons prop value))))

    (define (read-node)
      (sw!)
      (check-too-short!)
      (and (char=? #\; (at pos))
           (begin
             (bump!)
             (let loop ((acc '()))
               (cond ((read-property)
                      => (lambda (prop/value)
                           (loop (cons prop/value
                                       acc))))
                     (else
                      (reverse! acc)))))))

    (define (read-gametree n)
      (sw!)
      (let ((first-node? (zero? n))
            (nseq '())                  ; node sequence
            (kids '()))                 ; sub gametrees

        (define (another-node! node)
          (set! nseq (cons node nseq)))

        (define (another-kid! subtree)
          (set! kids (cons subtree kids)))

        (let loop ((cur (at pos)))
          (case cur
            ((#\;)
             (let ((node (read-node)))
               (and first-node?
                    (let ((sz (or (get-one node 'SZ)
                                  19)))
                      (define (large? n)
                        (< 19 n))
                      (set! first-node? #f)
                      (and (if (pair? sz)
                               (and (large? (car sz))
                                    (large? (cdr sz)))
                               (large? sz))
                           (fluid-set! tt-means-PASS? #f))))
               (sw!)
               (another-node! node)
               (loop (at pos))))
            ((#\()
             (bump!)
             (let ((subtree (read-gametree (1+ n))))
               (another-kid! subtree)
               (loop (at pos))))
            ((#\))
             (bump!)
             (cons (reverse! nseq)
                   (reverse! kids)))
            (else
             (sw!)
             (loop (at pos)))))))

    ;; do it!
    (let ((collection '()))
      (let loop ()
        (sw!)
        (or (and (not (= len pos))
                 (char=? #\( (at pos))
                 (begin
                   (bump!)
                   (set! collection (cons (with-fluids ((tt-means-PASS? #t))
                                            (read-gametree 0))
                                          collection))
                   (loop)))
            (reverse! collection))))))

;;;---------------------------------------------------------------------------
;;; game tree utilities

;; If @var{x} is @code{#f} or the empty list, return @code{#f}.
;; Otherwise, return @var{x}.
;; The name stands for ``null not appreciated''.
;;
(define (nna x)
  (and x (not (null? x)) x))

;; Return a list of children game trees for @var{tree},
;; or #f if @var{tree} has no children.
;;
(define (children tree)                 ; => #f if none
  (nna (cdr tree)))

;; Return a list of nodes (minimum is one) for @var{tree}.
;;
(define (nodes tree)                    ; => list (minimum length 1)
  (car tree))

;; If @var{node} is a move, return a pair @code{(@var{player} . @var{pos})},
;; where @var{player} is either @code{B} or @code{W} (a symbol), and
;; @var{pos} is the position of the move (either a two-element vector,
;; or the symbol @code{PASS}).
;;
;; If @var{node} has both @code{B} and @code{W} (not compliant with SGF),
;; return the @code{B} pair.  If @var{node} has neither, return @code{#f}.
;;
(define (node-move node)
  (or (assq 'B node)
      (assq 'W node)))

;; In @var{node}, return the value associated with the first occurance
;; of @var{prop}.
;;
(define (get-one node prop)
  (assq-ref node prop))

;;; ff-r.scm ends here
