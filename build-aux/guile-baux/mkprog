#!/bin/sh
exec ${GUILE-guile} -e '(guile-baux mkprog)' -s $0 "$@" # -*-scheme-*-
!#
;;; mkprog --- concat header / initial comments / body, chmod +x

;; Copyright (C) 2012 Thien-Thi Nguyen
;;
;; This file is part of SGF Utils.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with SGF Utils.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Usage: mkprog -o PROG PRIMARY MORE...
;;
;; Create PROG, chmod +x, with shell script header, initial
;; comments from PRIMARY, and the contents of MORE... files.

;;; Code:

(define-module (guile-baux mkprog)
  #:export (main)
  #:use-module ((guile-baux common) #:select (fs fso check-hv qop<-args))
  #:use-module ((srfi srfi-13) #:select (string-join)))

(define (rest prog primary . more)

  (define (s! s . args)
    (system (string-append ">> " prog " " (apply fs s args))))

  (s! "sed '1,/;;; Code:/!d;1s/.scm//' ~A" primary)
  (s! "cat ~A" (string-join more))
  (s! "echo ';;; ~A ends here'" prog)
  (system (fs "chmod +x ~A" prog)))

(define (main/qop qop)
  (let ((prog (qop 'output)))
    (with-output-to-file prog
      (lambda ()
        (fso "#!/bin/sh~%")
        (fso "exec ${GUILE-guile} -e '(~A)' -s $0 \"$@\" # -*-scheme-*-~%"
             prog)
        (fso "!#~%")))
    (apply rest prog (qop '()))
    #t))

(define (main args)
  (check-hv args '((package . "SGF Utils")
                   (version . "1.0")
                   (help . commentary)))
  (exit
   (main/qop (qop<-args
              args '((output (single-char #\o) (value #t)
                             (required? #t)))))))

;;; mkprog ends here
