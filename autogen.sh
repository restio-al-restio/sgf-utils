#!/bin/sh
# Usage: ./autogen.sh

[ -f src/sgfc.scm ] || {
  echo "autogen.sh: run this command only in the sgf-utils directory."
  exit 1
}

set -ex

######################################################################
# Guile-BAUX

guile-baux-tool snuggle m4 build-aux
guile-baux-tool import \
    common \
    gbaux-do

######################################################################
# Invoke the auto* tools.

autoreconf -B $(guile-config info datadir)/aclocal -v -i -f

######################################################################
# Done.

: Now run configure and make.

# autogen.sh ends here
